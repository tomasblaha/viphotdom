<?php
require_once('Classes/Debug.php');

require_once('config/config.php');

require_once('Classes/Input.php');
require_once('Classes/Database.php'); // Database connection class
require_once('Classes/Mail.php'); // Database connection class
require_once('Classes/Login.php'); // Class for loggin in, checking user status
require_once('Classes/User.php');
require_once('Classes/Page.php');

$debug = new Debug();
$debug->getConsole();