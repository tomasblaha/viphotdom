<?php
/*
 * Login form file
 */

require_once 'includes/autoload.php';
$login = new Login(0);

if (isset($_POST['userLoginSubmit'])) {
    $login->login(); // User is not logged, check form submit
}


$page = new Page("Login");
$page->getHeader();

if ($login->hasErrors())
    $login->getErrors();
?>

<h1>Login</h1>

<form method="POST" action="">
    <input type="email" placeholder="E-mail" name="userLoginEmail">
    <input type="password" placeholder="Password" name="userLoginPassword">
    <input type="submit" name="userLoginSubmit" value="Login">
</form>