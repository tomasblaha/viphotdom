<?php

class Login extends Database {

    const LOGINSESSIONEXPIRATION = "10 MINUTE"; // Time, when session gets deleted from database and become invalid
    const LOGINMAXATTEMPTS = 10; //Number of maximum login attempts per time limit to blocok the user account
    const LOGINATTEMPTSTIME = "10 MINUTE"; // Time when unseccessful attempts for logging in are count
    const LOGINURL = "/login";
    const LOGGEDURL = "/index";
    const LOGOUTURL = "/logout";
    const REGISTERURL = "/registration";
 
    private $userid;
    private $logged = false;
    private $messages;
    private $onlyLogged;

    /*
     * onlyLogged
     *  0 - only not-logged | redirect logged users
     *  1 - neutral | no redirect
     *  2 - only logged | redirect not-logged users
     */
    public function __construct($onlyLogged = 1) {
        $this->messages["error"] = array();
        $this->messages["message"] = array();
        $this->onlyLogged = $onlyLogged;

        parent::__construct(); // connect PDO

        if (session_status() == PHP_SESSION_NONE) {
            session_start(); // If sessions not started yet, then start
        }


        if (!$this->logged) {
            $this->checkSession(); // Try login using stored sessions
        }

        if (!$this->logged) {
            //$this->checkCookie();
        }




        if (!$this->logged) {
            if ($onlyLogged == 2) { // Current page is available only for logged users, redirect away
                header("Location: " . HOMEURL . Login::LOGINURL);
                die();
            }
        } else {
            if ($onlyLogged == 0) { // Current page is not available for logged users, redirect away
                header("Location: " . HOMEURL . Login::LOGGEDURL);
                die();
            }
        }
    }
    
    public function factory(){
        return new Login();
    }

    public function hasErrors() {
        return is_array($this->messages["error"]);
    }

    public function hasMessages() {
        return is_array($this->messages["message"]);
    }

    public function getErrors() {
        foreach ($this->messages["error"] as $message) {
            echo "<div class='error-message'>" . $message . "</div>";
        }
    }

    public function getMessages() {
        foreach ($this->messages["message"] as $message) {
            echo "<div class='success-message'>" . $message . "</div>";
        }
    }

    private function logError($error) {
        array_push($this->messages["error"], $error);
    }

    private function logMessage($message) {
        array_push($this->messages["message"], $message);
    }

    // Create session for user login
    private function createSession() {
        $token = $this->createUserLoginSessionToken(); // unique token identifying session

        $_SESSION['userid'] = $this->userid;
        $_SESSION['userip'] = $_SERVER['REMOTE_ADDR'];
        $_SESSION['useragent'] = $_SERVER['HTTP_USER_AGENT'];
        $_SESSION['usersessiontoken'] = $token;

        // Insert session information into DB for future verification
        $this->createUserLoginSessionDb($this->userid, $_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT'], $token);
    }

    // Insert user session information into DB
    private function createUserLoginSessionDb($userid, $userip, $useragent, $usersessiontoken) {
        $stmt = $this->pdo->prepare("INSERT INTO " . Database::USERLOGINSESSIONTABLE . " (userid, userip, useragent, usersessiontoken) VALUES (:userid, :userip, :useragent, :usersessiontoken)");
        $stmt->bindParam(':userid', $userid);
        $stmt->bindParam(':userip', $userip);
        $stmt->bindParam(':useragent', $useragent);
        $stmt->bindParam(':usersessiontoken', $usersessiontoken);
        return $stmt->execute();
    }

    // Returns unique token for user login session identification
    private function createUserLoginSessionToken() {
        $token = md5($this->userid . $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT'] . date("ymdhis"));
        return $token;
    }

    // Delete old user session from DB
    private function deleteUserOldLoginSessionDb() {
        $stmt = $this->pdo->prepare("DELETE FROM " . Database::USERLOGINSESSIONTABLE . " WHERE userid = :userid AND DATE_SUB(CURRENT_TIME(), INTERVAL " . Login::LOGINSESSIONEXPIRATION . ") > created");
        $stmt->bindParam(':userid', $this->userid);
        return $stmt->execute();
    }
    
    public function checkLogged(){
        if($this->checkSession()){
            return true;
        }
        
        //Check cookies
        
        return false;
    }

    // Check current session, verify in DB and try to login
    private function checkSession() {
        // Check is sessions are stored
        if (
                !isset($_SESSION['userid']) ||
                !isset($_SESSION['userip']) ||
                !isset($_SESSION['useragent']) ||
                !isset($_SESSION['usersessiontoken'])
        ) {
            // Cancel if sessions are not stored
            return false;
        }

        // Check if user in sessions is valid
        if ($this->validateUserId($_SESSION['userid'])) {

            $this->userid = $_SESSION['userid']; // User exists, set user id from session
            $this->deleteUserOldLoginSessionDb(); // Delete expired session from DB
            // Get all valid user session from DB
            $stmt = $this->pdo->prepare("SELECT userid,userip,useragent,usersessiontoken FROM " . Database::USERLOGINSESSIONTABLE . " WHERE userid = :userid AND userip = :userip AND useragent = :useragent");
            $stmt->bindParam(':userid', $this->userid);
            $stmt->bindParam(':userip', $_SERVER['REMOTE_ADDR']);
            $stmt->bindParam(':useragent', $_SERVER['HTTP_USER_AGENT']);
            $stmt->execute();
            $userSessionsDb = $stmt->fetchAll();
            $userSessionsDbCount = count($userSessionsDb);

            // Check if there are some valid sessions in DB
            if ($userSessionsDbCount == 0) {
                // Unset current login sessions
                unset($_SESSION['userid']);
                unset($_SESSION['userip']);
                unset($_SESSION['useragent']);
                unset($_SESSION['usersessiontoken']);

                return false; // No valid sesion
            }

            //Check stored session, real information and DB data
            foreach ($userSessionsDb as $userSessionDb) {
                if (
                        $_SESSION['userid'] == $userSessionDb['userid'] && // Session user ID = DB user ID
                        $_SERVER['REMOTE_ADDR'] == $_SESSION['userip'] && // Current IP = Session IP
                        $_SERVER['REMOTE_ADDR'] == $userSessionDb['userip'] && // Current IP = DB IP
                        $_SESSION['userip'] == $userSessionDb['userip'] && // Session IP = DB IP
                        $_SERVER['HTTP_USER_AGENT'] == $_SESSION['useragent'] && // Current user agent = Session user agent
                        $_SERVER['HTTP_USER_AGENT'] == $userSessionDb['useragent'] && // Current user agent = DB user agent
                        $_SESSION['useragent'] == $userSessionDb['useragent'] && // Session user agent = DB user agent
                        $_SESSION['usersessiontoken'] == $userSessionDb['usersessiontoken'] // Session token = DB token
                ) {
                    // Found valid token - set userid and login
                    $this->userid = $_SESSION['userid'];
                    $this->logged = true;
                    return true;
                }
            }
            return false; // No valid token found in DB, dont login
        } else {
            return false; // User session userid is not valid
        }
        return false; // Session login not successful - cancel
    }

    // CHeck if user id exists
    private function validateUserId($id) {
        $stmt = $this->pdo->prepare("SELECT * FROM " . Database::USERTABLE . " WHERE ID = :id");
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        $count = count($stmt->fetchAll());
        if ($count == 1) {
            return true; // Userid exists
        } else {
            return false; // Userid not found in DB
        }
    }

    // Login user through login form
    public function login() {
        $email = $_POST['userLoginEmail']; // To-Do Sanitize
        $password = $_POST['userLoginPassword']; //To-Do Sanitize
        // Check if user email exosts in DB, if so - get password hash
        $stmt = $this->pdo->prepare("SELECT ID, userpassword, verified FROM " . Database::USERTABLE . " WHERE useremail = :useremail LIMIT 1");
        $stmt->bindParam(':useremail', $email);
        $stmt->execute();
        $user = $stmt->fetch();

        // User found in DB - check provided credentials
        if ($user) {

            if ($user["verified"] != 1) {

                $this->logError("Account not verified yet");
                return false;
            }

            // Check login attempts in DB
            $stmt = $this->pdo->prepare("SELECT ID FROM " . Database::USERLOGINATTEMPTS . " WHERE userid = :userid AND DATE_SUB(CURRENT_TIME(), INTERVAL " . Login::LOGINATTEMPTSTIME . ") < created");
            $stmt->bindParam(':userid', $user['ID']);
            $stmt->execute();
            $attempts = count($stmt->fetchAll());

            // Check user wrong login attempts
            if ($attempts < Login::LOGINMAXATTEMPTS) {

                // Login attempts didn't exceeded maximum - try to login
                if (password_verify($password, $user['userpassword'])) {
                    $stmt = $this->pdo->prepare("DELETE FROM " . Database::USERLOGINATTEMPTS . " WHERE userid = :userid");
                    $stmt->bindParam(':userid', $user['ID']);
                    $stmt->execute();
                    // Password matches hash - login
                    $this->userid = $user['ID'];
                    $this->logged = true;
                    $this->createSession();


                    if (!$this->onlyLogged) { // Current page is available only for logged users, redirect away
                        header("Location: " . HOMEURL . Login::LOGGEDURL);
                        die();
                    }

                    return true;
                } else {
                    // Password didn't match the hash - write wrong login attempt into DB
                    $stmt = $this->pdo->prepare("INSERT INTO " . Database::USERLOGINATTEMPTS . " (userid) VALUES (:userid)");
                    $stmt->bindParam(':userid', $user['ID']);
                    $stmt->execute();
                    $this->logError("Incorrect password.");
                    return false;
                }
            } else {
                $this->logError("Incorrect login limit exceeded. Try it again later.");
                return false; // Login attempts exceeded maximum - don't allow the login attempt
            }
        } else {
            $this->logError("Incorrect e-mail. User wasn't found.");
            return false; // User wasn't found in DB
        }
        $this->logError("Login wasn't successful.");
        return false; // Login not successful
    }

    // Logout user
    public function logout() {

        // Delete current login session from DB - identify by token
        $stmt = $this->pdo->prepare("DELETE FROM " . Database::USERLOGINSESSIONTABLE . " WHERE userid = :userid AND userip = :userip AND useragent = :useragent AND usersessiontoken = :usersessiontoken");
        $stmt->bindParam(':userid', $_SESSION['userid']);
        $stmt->bindParam(':userip', $_SESSION['userip']);
        $stmt->bindParam(':useragent', $_SESSION['useragent']);
        $stmt->bindParam(':usersessiontoken', $_SESSION['usersessiontoken']);
        $stmt->execute();

        // Unset current login sessions
        unset($_SESSION['userid']);
        unset($_SESSION['userip']);
        unset($_SESSION['useragent']);
        unset($_SESSION['usersessiontoken']);

        // Redirect to login page
        header("Location: " . HOMEURL . Login::LOGINURL);
    }

    public function register() {

        $email = $_POST['userRegistrationEmail'];
        $email = trim($email);

        $password = $_POST['userRegistrationPassword'];
        $password = trim($password);

        $passwordCheck = $_POST['userRegistrationPasswordCheck'];
        $password = trim($password);

        if (!isset($email) || !isset($password) || !isset($passwordCheck)) {
            $this->logError("Fill in required fields");
            return false;
        }

        if ($password != $passwordCheck) {
            $this->logError("Password and password check don't match.");
            return false;
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->logError("This e-mail adress is not valid. Provide another one.");
        }

        $stmt = $this->pdo->prepare("SELECT ID FROM " . Database::USERTABLE . " WHERE useremail = :useremail LIMIT 1");
        $stmt->bindParam(':useremail', $email);
        $stmt->execute();
        $user = $stmt->fetch();

        if ($user) {
            $this->logError("This e-mail adress is already registered.");
            return false;
        }


        $verficationToken = md5($email . rand(0, 9999) . date("YmdHis"));
        $password = password_hash($password, PASSWORD_BCRYPT);

        $stmt = $this->pdo->prepare("INSERT INTO " . Database::USERTABLE . " (useremail, userpassword, verificationtoken) VALUES (:useremail, :userpassword, :verificationtoken)");
        $stmt->bindParam(':useremail', $email);
        $stmt->bindParam(':userpassword', $password);
        $stmt->bindParam(':verificationtoken', $verficationToken);
        $stmt->execute();

        $email = urlencode($email);
        $verficationToken = urlencode($verficationToken);

        //$mail = new Mail();
        //$mail->sendVerificationToken($email, $verficationToken);


        echo(HOMEURL . Login::REGISTERURL . "?userverificationlink&useremail=$email&verificationtoken=$verficationToken");

        $this->logMessage("Registration was successful. Check your e-mail for verification link.");
    }

    public function verify() {

        if (!isset($_GET['useremail']) || !isset($_GET['verificationtoken'])) {
            return false;
        }

        $useremail = $_GET['useremail'];
        $verificationtoken = $_GET['verificationtoken'];


        $useremail = urldecode($useremail);
        $verificationtoken = urldecode($verificationtoken);


        $stmt = $this->pdo->prepare("SELECT ID FROM " . Database::USERTABLE . " WHERE useremail = :useremail AND verificationtoken = :verificationtoken LIMIT 1");
        $stmt->bindParam(':useremail', $useremail);
        $stmt->bindParam(':verificationtoken', $verificationtoken);
        $stmt->execute();
        $user = $stmt->fetch();

        if ($user) {
            $stmt = $this->pdo->prepare("UPDATE " . Database::USERTABLE . " SET verified = 1, verificationtoken = '' WHERE useremail = :useremail");
            $stmt->bindParam(':useremail', $useremail);
            $stmt->execute();
            $this->logMessage("Your account was verified. You can login");
            return true;
        } else {
            return false;
        }
    }

}
