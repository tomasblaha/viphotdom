<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Page
 *
 * @author tomas
 */
class Page {
    
    private $title;
    
    public function __construct($title = "VIP Hotdom") {
        $this->title = $title;
    }

    public function getHeader() {
        ?>
        <!DOCTYPE HTML>

        <html lang="en">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                <title><?php echo $this->title; ?></title>
            </head>
            <body>
                <?php
            }

            public function getFooter() {
                ?>
            </body>
        </html>
        <?php
    }

}
