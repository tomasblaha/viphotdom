<?php

require_once 'Libraries/PHPMailer/PHPMailerAutoload.php';

class Mail {

    const FROMEMAILADRESS = "no-reply@viphotdom.com";
    const EMAILWORDWRAP = 50;

    private $mail;

    public function __construct() {
        $this->mail = new PHPMailer();

        /*
        $this->mail->IsSMTP();
        $this->mail->SMTPAuth = true;
        $this->mail->Host = "smtp-108274.m74.wedos.net";
        $this->mail->Port = 587;
        $this->mail->Username = "viphotdom@tblaha.cz";
        $this->mail->Password = "HHeesslloo123$";
        $this->mail->SMTPSecure = 'tls';
        $this->mail->SetFrom(Mail::FROMEMAILADRESS, 'VIP Hotdom');
        $this->mail->AddReplyTo(Mail::FROMEMAILADRESS, "VIP Hotdom");
        */
        
        $this->mail->isMail();
         

        $this->mail->SMTPDebug = 1;
    }

    public function send($to, $subject, $body) {
        $this->mail->AddAddress($to);
        $this->mail->Subject = $subject;
        $this->mail->MsgHTML = $body;
        $this->mail->WordWrap = Mail::EMAILWORDWRAP;

        return $this->mail->Send();
    }

    public function sendVerificationToken($to, $token) {
        $this->mail->AddAddress($to);
        $this->mail->Subject = "VIP Hotdom account verification";
        $this->mail->Body = "Verify your VIP Hotdom account: ";
        $this->mail->WordWrap = Mail::EMAILWORDWRAP;
        $this->mail->IsHTML(true);

        return $this->mail->Send();
    }

}
