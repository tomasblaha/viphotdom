<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author tomas
 */
class User extends Database {
    
    private $id;
    
    public function __construct() {
        parent::__construct();

        // Object cant be created when user is not logged!
        if(!Login::factory()->checkLogged()){
            die();
        }
    }
    
}
