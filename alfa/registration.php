<?php
/*
 * Registration form file
 */

require_once 'includes/autoload.php';
$login = new Login(0);

if (isset($_GET['userverificationlink'])) {
    $login->verify();
}

if (isset($_POST['userRegistrationSubmit'])) {
    $login->register();
}

if ($login->hasErrors())
    $login->getErrors();
if ($login->hasMessages())
    $login->getMessages();
?>

<h1>Registration</h1>

<form method="POST" action="">
    <input type="email" placeholder="E-mail" name="userRegistrationEmail">
    <input type="password" placeholder="Password" name="userRegistrationPassword">
    <input type="password" placeholder="Password check" name="userRegistrationPasswordCheck">
    <input type="submit" name="userRegistrationSubmit" value="Register">
</form>